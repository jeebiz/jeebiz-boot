/** 
 * Copyright (C) 2018 Hiwepy (http://hiwepy.io).
 * All Rights Reserved. 
 */
package io.hiwepy.boot.demo.service;

import io.hiwepy.boot.api.service.IBaseService;
import io.hiwepy.boot.demo.dao.entities.DemoEntity;

public interface IDemoService extends IBaseService<DemoEntity>{

	
}
